__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2019. All rights are reserved.'
__license__ = 'GPL 3'

import requests
import json

def connect_instance(url: str, user: str, password: str) -> dict:
    """
    Connect to PD instance
    """
    url = f"{url}/PD/oauth/token"

    payload = f'grant_type=password&username={user}&password={password}'
    headers = {
      'Authorization': 'Basic b2F1dGgyV3NDbGllbnQ6b2F1dGgyV3NQYXNzd29yZA==',
      'Content-Type': 'application/x-www-form-urlencoded'
    }

    response = requests.request("POST",
                        url,
                        headers=headers,
                        data = payload,
                        auth=('oauth2WsClient', 'oauth2WsPassword'))
    return response


def get_projects(url: str, token_auth: str) -> list:
    """
    """
    url = f'{url}/PD/rest/v0/projects'
    params = (
            ('orderBy', 'projectId'),
            ('pageSize', '50'),
        )
    headers = {
        'accept': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    
    response = requests.get(url, headers=headers, params=params)
    return response


def get_project(url: str, proj_id: str, token_auth: str) -> dict:
    """
    Get a project
    """
    url = f'{url}/PD/rest/v0/projects/{proj_id}'
    headers = {
        'accept': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    
    response = requests.get(url, headers=headers)
    return response


def get_submissions(url: str, token_auth: str) -> list:
    """
    get the submissions of a project instance.
    """
    url = f'{url}/PD/rest/v0/submissions'
    headers = {
        'accept': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    
    response = requests.get(url, headers=headers)
    return response


def get_submission(url: str, submission_id: str, token_auth: str) -> dict:
    """
    Get a submission
    """
    url = f'{url}/PD/rest/v0/submissions/{submission_id}'
    headers = {
        'accept': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    
    response = requests.get(url, headers=headers)
    return response

def create_submission(url: str, token_auth: str, job_number: str, due_date: int, src_lang: str='en-US', tgt_lang='de-DE'):
    """
    Create an empty submission
    """
    url = f'{url}/PD/rest/v0/submissions/create'
    headers = {
        'Content-Type': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    
    payload = {
        'instructions': '', 
        'projectId': 8, 
        'dueDate': due_date, 
        'paJobNumber': job_number, 
        'paClientId': 16, 
        'claimScope': 'LANGUAGE', 
        'name': 'Api_test', 
        'sourceLanguage': src_lang,
        'batchInfos': [
                {
                'targetFormat': 'TXLF', 
                'name': 'Batch1', 
                'targetLanguageInfos': [
                    {
                        "targetLanguage": "de-DE"
                    }
                ]
                }
            ], 
        }
    response = requests.post(url, headers=headers, data=json.dumps(payload))
    return response

def add_source_files(url: str,token_auth: str,  sub_id: str, file: str, file_format: str, batch_name: str='Batch1'):
    """
    Upload a source file to the sub
    """
    url = f'{url}/PD/rest/v0/submissions/{sub_id}/upload/source'
    headers = {
        'Content-Type': 'application/json', 
        'Authorization': f'Bearer {token_auth}'}
    payload = {'BatchName': f'{batch_name}', 
            'fileFormatName': file_format}
    files = [('file', open(file, 'rb'))]
    return requests.post(url, headers=headers, data=payload, files=files)

def download_source_files(url: str, token_auth:str, sub_id: int, phase_name: str='Translation'):
    """
    Download files
    """
    full_url = f'{url}/PD/rest/v0/submissions/{sub_id}/download?' \
            f'sourceFiles=true'
    headers = {'Authorization': f'Bearer {token_auth}'}
    res = requests.get(full_url, headers=headers)
    download_id = res.json()['downloadId']
    print(download_id)
    return res

def check_download(url: str, token_auth: str, sub_id: int, download_id: str) -> bool:
    """
    Check if the download is ready to download.
    Arguments:
        url (str): Url of the PD instance.
        token_auth (str): The authentication token generated.
        sub_id (str): The sub to generate the download.
        download_id(str): The download id to make the download of the files.
    """
    full_url = f'{url}/PD/rest/v0/submissions/{sub_id}/download?downloadId={download_id}'
    headers = {'Authorization': f'Bearer {token_auth}'}
    print(full_url)
    #params = {'downloadId': f'{download_id}'}
    res = requests.get(full_url, headers=headers)
    print(res.json().get('processingFinished'))
    if res.json().get('processingFinished'):
        return True
    else:
        import time;time.sleep(2)
        return check_download(url, token_auth, sub_id, download_id)

def download_content(url: str, token_auth: str, download_id: str) -> dict:
    """
    """
    full_url = f'{url}/PD/rest/v0/submissions/download/{download_id}'
    headers = {'Authorization': f'Bearer {token_auth}'}
    
    res = requests.get(full_url, headers= headers)
    return res
