from setuptools import setup
setup(name='pdf_library', 
        version='0.1', 
        description='Small library to detect language of a text', 
        long_description='It detects the language of the passed text. It allows to use different libraries', 
        classifiers=[
            'Development Status :: 3 - Alpha', 
            'License :: OSI Approved :: LGPL License 3', 
            'Programming Language :: Python :: 3.7.5', 
            'Topic :: PDF processing :: text processing'
        ], 
        keywords='PDF text', 
        author='Llorenç Suau', 
        author_email='lsuau@translations.com', 
        license='LGPL 3', 
        packages=['pdf_library'], 
        zip_safe=False)
