"""
PDF Library module.
It is a unique file library to process PDF files and make basic operations on them.
"""
__author__ = 'Llorenç Suau Cànaves'
__copyright__ = 'Copyright (c) 2019. All rights are reserved.'
__license__ = 'GPL 3 or better'

import fitz
from pathlib import Path
import xlsxwriter

class Page:
    """
    Represent a page of the document.
    
    Properties:
        text (str): The text existing in the page.
        html (str): Extracts the HTML contents of the page. For visualization.
        json (str): Extracts the text in JSON format.
        pixmap (str): Extracts the image.
    """
    def __init__(self, page: fitz.Page) -> None:
        """
        Initialize and construct the object.
        
        Arguments:
            page (fitz.Page): A page parsed through fitz library.
        """
        self._page = page
        self.number = self._page.number
        self._display_list = page.getDisplayList()
        self._text_page = self._display_list.getTextPage()
        
        self._text = self._text_page.extractText()
        self._html = self._text_page.extractHTML()
        self._json = self._text_page.extractJSON()

    @property
    def text(self) -> str:
        """
        The text of the page
        """
        return self._text

    @property
    def html(self) -> str:
        """
        Export the text in HTML format containing its format.
        """
        return self._html

    @property
    def json(self) -> str:
        """
        Export the text in JSON format keeping its format information. 
        """
        return self._json

    @property
    def pixmap(self) -> fitz.Pixmap:
        """
        Extracts the image of the page.
        """
        return self._page.getPixmap()

    def highlight_text(self, text: str):
        """
        Highlight the text based on text
        """
        text_instances = self._text_page.search(text, quads=True)
        self._page.addHighlightAnnot(text_instances)
        return text_instances


class PDF:
    """
    Represents a full PDF document.
    
    Properties:
        text (str): Shows the text of the whole document.
    """
    def __init__(self, file: str):
        """
        """
        if Path(file).is_file():
            self._file_name = file
            self.doc = fitz.open(file)
        self._pages = [Page(p) for p in self.doc]
        

    @property
    def text(self, max_chars: int=0) -> str:
        """
        Return all the text of the document
        """
        text = ''
        for page in self.pages:
            page_size = len(page.text)
            total_size = len(text) + page_size
            actual_size = len(text)
            if max_chars > 0:
                if total_size <= max_chars:
                    text += page.text
                else:
                    size_needed = max_chars - actual_size
                    text += page.text[:size_needed]
            else:
                text += page.text
        return text

    @property
    def file_name(self) -> str:
        """
        The file name of the PDF file.
        """
        return self._file_name
    
    @property
    def pages(self) -> Page:
        """
        Contains all the pages of the document.
        """
        return self._pages
        
    @property
    def count_chars(self) -> int:
        """
        Count the number of characters in the document.
        """
        return len(self.text)

    def highlight_text(self, text: str):
        """
        Highlight the text based on text
        
        Arguments:
            text (str): The text value that needs to be highlighted in the document.
        """
        all_text_instances = list()
        for pi in range(self.doc.pageCount):
            page = self.doc[pi]
            text_instances = page.searchFor(text, quads=True)
            page.addHighlightAnnot(text_instances)
            all_text_instances.append(text_instances)
        return all_text_instances
    
    def close(self):
        """
        Closes the pdf
        """
        self.doc.close()
    
    def save(self, close: bool=True):
        """
        Save any changes of the document.
        
        Arguments:
            close (bool): Indicate if the documents after saving needs to be closed. By default True.
        """
        self.doc.save(self.file, incremental=True, encryption=fitz.PDF_ENCRYPT_KEEP)
        if close:
            self.doc.close()

def generate_instances_export(file: str, text: str, text_instances: list) -> list:
    """
    Generate the export of instances of text.
    """
    res = list()
    for instance in text_instances:
        if instance:
            found = True
            break
        else:
            found = False        
    res.append(file)
    res.append(text)
    res.append(found)
    return res

def write_excel(report_file: str, results: list) -> None:
    """
    Generate an excel report
    
    Arguments:
        report_file (str): Path to the file where be saved the changes.
        results (list): List of instances that are needed to be exported.
    """
    wb = xlsxwriter.Workbook(report_file)
    ws = wb.add_worksheet()
    ws.write(0, 0, 'File')
    ws.write(0, 1 , 'String')
    ws.write(0, 2 , 'Is found?')
    row = 1
    not_found_format = wb.add_format({'bg_color': '#FF0000'})
    for file, text, found in (results):
        ws.write(row, 0, file)
        ws.write(row, 1, text)
        if found:
            ws.write(row, 2, 'Highlighted')
        else:
            ws.write(row, 2, 'Not found')
            ws.set_row(row, cell_format=not_found_format)
        row += 1
    wb.close() 

if __name__ == '__main__':
    file = r'q:\zz_Temp\lsuau\Development\Python\pdf_language_sorter\files\unsorted\Coelho_2018.pdf'
    pdf = PDF(file)
    pdf.read_pdf()
    import pprint
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(pdf.contents)
    print(pdf.count_chars)
