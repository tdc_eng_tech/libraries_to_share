from setuptools import setup
setup(name='office_library', 
        version='0.2.25', 
        description='Library to process Office files', 
        long_description='It allows to work with Excel files', 
        classifiers=[
            'Development Status :: 3 - Alpha', 
            'License :: OSI Approved :: LGPL License 3', 
            'Programming Language :: Python :: 3.6.4', 
            'Topic :: Text Processing :: Translation'
        ], 
        keywords='office excel word', 
        author='Llorenç Suau', 
        author_email='lsuau@translations.com', 
        license='LGPL 3', 
        packages=['office_library','office_library.excel'], 
        zip_safe=False)
