"""
Excel module.


It allows to handle Excel files.
"""
__author__ = 'Llorenç Suau'
__copyright__ = 'Copyright (c) 2018. All rights are reserved'
__license__ = 'GPL 3 or better'


import xlrd
# from xlutils.copy import copy
from pathlib import Path
# import xlwt
from win32com.client.gencache import EnsureDispatch as Dispatch
import logging
from enum import Enum
from system_tools import system_tools
import string

logging.basicConfig(filename='excel.log', 
    level=logging.DEBUG, 
    format='%(asctime)s - %(levelname)s - %(message)s'
    )

def clamp(x):
    """
    Internal function to calculate the max, min to be used in the
    rgb2hex function.
    Arguments:
        x (int): Numeric value between 0 and  255
    Returns:
        An integer between 0 and 255
    """
    return max(0, min(x, 255))


def rgb2hex(tup_value):
    """
    Converts a RGB tuple into an hexadecimal color

    Arguments:
        tup_value (tuple): Value to convert into hexadecimal

    return:
        hex_value (int): Integer in hexadecimal value
    """
    if tup_value:
        if len(tup_value) == 3:
            return '#{0:02x}{1:02x}{2:02x}'.format(
                             clamp(tup_value[0]),
                             clamp(tup_value[1]),
                             clamp(tup_value[2])
                             )           

class XlFileFormat(Enum):

    """
    Contains the numeric values of the different 
    Excel format that are used by Excel using the COM library.
    """
    xlsx = 51 # xlOpenXMLWorkbook
    xlsm = 52 #xlOpenXMLWorkbookMacroEnabled
    xls = 56  #xlExcel8
    xml = 46 # xlXMLSpreadsheet

class Color(Enum):
    """
    Main colors easy to identify instead of having to use
    RGB tuples.
    red, black, white, green, green_pampers, green_pampers_write,
    blue, yellow, grey
    """
    red = (255, 0, 0)
    black = (0, 0, 0)
    white = (255, 255, 255)
    green = (0, 255, 0)
    green_pampers = (146, 208, 80)
    green_pampers_write = (153, 204, 0)
    blue = (0, 0, 255)
    yellow = (255, 255, 0)
    grey = (128, 128, 128)

class HexColor(Enum):
    """
    Colors in Hexadecimal format #fffffff
    """
    red = rgb2hex(Color.red.value)
    black = rgb2hex(Color.black.value)
    white = rgb2hex(Color.white.value)
    green = rgb2hex(Color.green.value)
    green_pampers = rgb2hex(Color.green_pampers.value)
    green_pampers_write = rgb2hex(Color.green_pampers_write.value)
    blue = rgb2hex(Color.blue.value)
    yellow = rgb2hex(Color.yellow.value)
    grey = rgb2hex(Color.grey.value)

class Font:
    """
    Contain information about the font of a text. It has to be used
    within segment or cell class. Not directly instantiated.

    Properties:
        size(int): Returns the size of the font in points

    Attributes:
        name(str): Name of the font as Arial, Times New Roman,...
        color(tuple): Stores the color as an RGB tuple (0,255,0) as green.
        _color_hex(str): The color in hexadecimal form
        bold(bool): Sets the font as bold or not (True/False). True is enabled.
        italic(bool): Sets the font as italic (True/False). True is enabled
        underline(bool): Sets the font as underlined (True/False). True is enabled
        superscript(bool): Sets the font as superscript (True/False). True is enabled
        underscript(bool): Sets the font as underscript (True/False). True is enabled
    """

    def __init__(self, name, size, color, bold, italic, underline, escapement):
        """
        Initialization of the class Font.

        Arguments:
            name(str): Name of the font
            size(int): Size of the font in twips (from xlrd)
            bold(int): 0/1 It sets the boldeness of the font
            italic(int): 0/1 sets italic font
            underline(int): 0/1 sets the underline
            escapement(int): Sets if the font is subscript or superscript
        """
        self.name = name
        self._size = size
        self.color = color
        if color:
            self._color_hex = rgb2hex(color)
        else:
            self._color_hex = '#000000'
        self.bold = bool(bold)
        self.italic = bool(italic)
        self.underline = bool(underline)
        self.superscript = False
        self.subscript = False
        if escapement == 0:
            self.superscript = False
            self.subscript = False
        elif escapement == 1:
            self.superscript = True
            self.subscript = False
        elif escapement == 2:
            self.subscript = True
            self.superscript = False

    @property
    def size(self):
        """
        Return the size of the font in points.
        """
        return int(self._size / 20)

    @property
    def color_hex(self):
        """
        Return the color of the font in hexadecimal string.
        """
        if self.color:
            return rgb2hex(self.color)
        else:
            return '#000000'

class BorderLineStyle(Enum):
    """
    """
    no_line = 0
    thin = 1
    medium = 2
    dashed = 3
    dotted = 4
    thick = 5
    double = 6
    hair = 7
    medium_dashed = 8
    thin_dash_dotted = 9
    medium_dash_dotted = 10
    thin_dash_dot_dotted = 11
    medium_dash_dot_dotted = 12
    slanted_medium_dash_dotted = 13

class Border:
    """
    Definition of the border of the cell. It has not to be instantiated directly.
    Access to it from Format class.
    
    Attributes:
        __xl_format(xlrd.Format): Private attribute for xlrd.format.
        __xl_border(xlrd.Border): Private object attribute of the border format.
        workbook (xlrd.Workbook): Contains the Workbook.
        bottom_color(tuple): Color of the bottom line.
        top_color(tuple): Color of the top line.
        left_color(tuple):Color of the left line.
        right_color(tuple):Color of the right line.
        color(tuple): Color of all lines if they have the same color.
        bottom_line(int): Type of line of the bottom line.
        top_line(int): Type of line of the top line.
        left_line(int): Type of the line of the left line.
        right_line(int): Type of the line of the right line.
        line_style(int): Type of all the lines if they have the same style.
    """ 
    def __init__(self, xl_format, workbook=None):
        """
        Initialization of the class.

        Arguments:
            xl_format(xlrd.Format): Format object of xlrd
            workbook (xlrd.Workbook): Workbook object if it applicable
        """
        self.__xl_format = xl_format
        if self.__xl_format:
            self.__xl_border = xl_format.border
        else:
            self.__xl_border = None
        self.workbook = workbook
        if self.workbook and self.__xl_format:
            self.bottom_color = self.workbook.workbook.colour_map[
                                       self.__xl_border.bottom_colour_index]
            self.top_color = self.workbook.workbook.colour_map[
                                  self.__xl_border.top_colour_index]
            self.left_color = self.workbook.workbook.colour_map[
                                  self.__xl_border.left_colour_index]
            self.right_color = self.workbook.workbook.colour_map[
                                    self.__xl_border.right_colour_index]
            self.color = self.__border_color_all()
            self.bottom_line = self.__border_line_definition(
                                     self.__xl_border.bottom_line_style)
            self.top_line = self.__border_line_definition(
                                     self.__xl_border.top_line_style)
            self.left_line = self.__border_line_definition(
                                     self.__xl_border.left_line_style)
            self.right_line = self.__border_line_definition(
                                     self.__xl_border.right_line_style)
            if (self.top_line == self.bottom_line
                 and self.top_line == self.left_line
                 and self.top_line == self.right_line):
                self.line_style = self.top_line
            else:
                self.line_style = None
        elif not self.__xl_border:
            self.bottom_color = BorderLineStyle.no_line
            self.top_color = BorderLineStyle.no_line
            self.left_color = BorderLineStyle.no_line
            self.right_color = BorderLineStyle.no_line
            self.color = None
            self.bottom_line = BorderLineStyle.no_line
            self.top_line = BorderLineStyle.no_line
            self.left_line = BorderLineStyle.no_line
            self.right_line = BorderLineStyle.no_line
            self.line_style = BorderLineStyle.no_line

    def __border_color_all(self):
        """
        Internal method that sets the border_color if all borders have the same color
        """
        if (self.bottom_color == self.top_color
              and self.bottom_color == self.left_color
              and self.bottom_color == self.right_color):
            return self.bottom_color
        else:
            return None

    def __border_line_definition(self, border_line):
        """
        Internal method that sets the type of border line if all the border lines are the same.
        """
        for line in BorderLineStyle:
            if line.value == border_line:
                return line

class Format:
    """
    Format of a cell or a segment of text in a cell if this cell has rich text.

    """
    def __init__(self, xl_format, font, workbook=None):
        """
        Initialization of format.
        
        Arguments:
            xl_format(xlrd.Format): Format of the cell from xlrd.
            font(xlrd.Font): Font object from xlrd.
            workbook(ExcelFile): ExcelFile object as reference.
        """
        self.xl_format = xl_format
        self.workbook = workbook
        self.font = font
        self.border = Border(xl_format, workbook)
        self._background_color = None
        self._background_pattern = None

    @property
    def background_color(self):
        """
        Return the background color in tuple format.
        """
        if self.xl_format:
            pattern_color = self.workbook.workbook.colour_map[
                                 self.xl_format.background.pattern_colour_index]
        else:
            # No Pattern color. It means auto.
            pattern_color = None
        self._background_color = pattern_color
        
        return pattern_color

    @property
    def background_color_hex(self):
        """
        Return the baclground color in hexadecimal format.
        """
        if self.background_color:
            return rgb2hex(self.background_color)
        else:
            return None

    @property
    def background_fill_pattern(self):
        """
        Return the background fill pattern
        """
        if self.xl_format:
            fill_pattern = self.xl_format.background.fill_pattern
        else:
            fill_pattern = 0
        self._background_pattern = fill_pattern

        return fill_pattern

class RichTextSegment:
    """
    Information of the segments of a cell that has different format. It is accessed
    from Cell object when the cell contains rich text.
    Accesible in Cell.segments.
    
    Attributes:
        text(str): The content of the segment of the cell with a format.
        workbook(ExcelFile): Workbook reference.
        worksheet(WorkSheet): Worksheet reference.
        cell(Cell): Cell backreference where this segment is contained.
        start(int): Position of the first character in the segment.
        end(int): Position of the last character in the segment.
        _xl_font(xlrd.Font): Private attribute of Font extracted from the workbook through xlrd.
        format(Format): Format of the segment
        
    Properties:
        length(int): Size of the segment. Total number of characters.
        color(tuple): Alias of format.font.color.
        size(int): Alias of format.font.size
    """
    
    def __init__(self, text, start, end,  xl_font, workbook=None, cell=None):
        """
        Initialization of RichTextSegment.
        Arguments:
            text(str): Text of the segment.
            start(int): Position of the first character in the segment.
            end(int): Position of the last character in the segment.
            xl_font(xlrd.Font): Font from the xlrd library.
            workbook(ExcelFile): Backreference to the workbook that contains it.
            cell(Cell): Cell backreference to the cell that contains it.
        """
        self.text = text
        self.workbook = workbook
        self.worksheet = None
        self.cell = cell
        self.start = start
        self.end = end
        self._xl_font = xl_font
        self.format = Format(self.cell.xl_format, Font(
                              xl_font.name, xl_font.height,
                              self.workbook.workbook.colour_map[
                                        xl_font.colour_index],
                              xl_font.bold, xl_font.italic,
                              xl_font.underline_type, xl_font.escapement
                             ), self.workbook)

    @property
    def length(self):
        """
        Returns the total number of characters of the segment.
        """
        return self.end - self.start

    @property
    def color(self):
        """
        Alias of format.font.folor
        """
        return self.workbook.colour_map(self.font.colour_index)

    @property
    def size(self):
        """
        Alias of format.font.size
        """
        return int(self._size / 20)

class Cell:
    """
    Information of a cell in a worksheet. Don't directly instantiate.
    It is accessible from worksheet.cells().
    
    Example:
        workbook = ExcelFile('sample.xls')
        print(workbook.worksheets[0].cells(name='D4').text)
    
    Attributes:
        _row_index(int): Private attribute of Row index in from 0 to n-1.
        _row(int): Private attribute of the row number from 1 to n.
        _column_id(int): Column number from 1 to n.
        _column_index(int): Column number from 0 to n-1.
        column(str): Column letter of the Column. For example ('A').
        xl_cell(xlrd.Cell): Contents of the cell from xlrd.
        merged(bool): Informs if the cell forms part of a merged cell or not.
        _notes(list): Private attribute that contains the notes of the cell.
                          This can come from other cell, it has no relation to Excel comments.
        workbook(ExcelFile): Backreference to the ExcelFile that contains the cell.
        worksheet(WorkSheet): Backreference to the sheet that contains the cell.
        _char_count(int): Private attribute that contains the number of characters of the cell.
        xl_format(xlrd.Format): Format of the cell. Xlrd format object.
        xl_font(xlrd.Font): Contains the information of the font of the text of the cell.
        font(Font): Alias of format.font
        format(Format): Format of the cell. Font, border and interior.
        is_copy(bool): If the cell has been copied from other cell or not.
        has_rich_text(bool): Is True when the cell has text with different formats.
        is_writable(bool): The cell can be written. 
        _maxlen(int): Private attribute of the max length of the cell.
        rich_text_list(list): List of segments that contain the rich text segment.
        _target_cells(list): List of cells thas been copied to this cell.
        _source_cell(Cell): Source cell from which has been copied the cell. If it is a copy.
    
    Properties:
        column_id(int): Returns the id of cell from 1 to n.
        row(int): Return the row number from 1 to n. 
        row_index(int): Return the row index from 0 to n-1.
        name(str): Return the cell name in format A1.
        value(str): Text of the cell, it contains all the segments together.
        segments(list): Contains the list of Segment objects.
        characters_count(int): Number of characters in the cell.
        background_color(int): Background color in tuple RGB representation.
        background_color_hex(str): Return the color of the cell as hexadecimal representation.
        notes(list): The notes of the cell
        maxlen(int): Maximum length of characters of the cell.
        background_fill_pattern(int): Pattern of the cell.
    
    Methods:
        add_note(value): Adds a note to the cell.
        remove_notes: Remove all the notes of the cell.
        copy(target_row, target_column, workbook=None, worksheet=None, is_writable=False): Copies the cell.
        _get_value(): Private method to get the value of the cell.
    """
    def __init__(self, row, column, cell=None, workbook=None,
                      worksheet=None, copy=False):
        """
        Initialization of the Cell object.
        Arguments:
            row(int): row from 0 to n-1.
            column(int): column from 0 to n-1.
            cell(xlrd.Cell): xlrd Cell object.
            workbook(ExcelFile): Backreference to workbook.
            worksheet(WorkSheet): Backreference to worksheet.
            copy(bool): Indicated if the cell is a copy from other one or not.
        """
        self._row_index = row
        self._row = row + 1
        self._column_id = column + 1
        self._column_index = column
        self._column = column_letter(self._column_id)
        self.xl_cell = cell
        self.merged = False
        self._notes = []
        self.workbook = workbook
        self.worksheet = worksheet
        self._char_count = 0
        self.xl_format = None
        self.format = None
        self.xl_font = None
        self.has_rich_text = False
        self.font = None
        self.is_copy = copy
        self.is_writable = False
        if not copy:
            self.xl_format = self.workbook.workbook.xf_list[
                                  self.worksheet.xl_worksheet.cell_xf_index(row, column)]
            self.xl_font = self.workbook.workbook.font_list[self.xl_format.font_index]
            self.format = Format(self.xl_format, Font(self.xl_font.name, self.xl_font.height,
                                  self.workbook.workbook.colour_map[
                                          self.xl_font.colour_index], self.xl_font.bold, 
                                  self.xl_font.italic, self.xl_font.underline_type,
                                  self.xl_font.escapement), self.workbook)
        self._maxlen = 0
        self._target_cells = []
        self._source_cell = None
        if cell:
            self.text = cell.value
        else:
            self.text = ''       
        self.hidden = False
        self._background_color = None
        self._background_pattern = None
        self.rich_text_list = {}
        if not copy:
            self.rich_text_list = self.worksheet.xl_worksheet.rich_text_runlist_map.get(
                                        (row, column))
        self._segments = self._get_value()
        #self.excel_app = None

    @property
    def column_id(self):
        """
        Return the column number of the cell from 1 to n.
        """
        return self._column_id

    @property
    def row(self):
        """
        Row number in form of 1 to n.
        """
        return self._row
    
    @property
    def column(self):
        """
        """
        return self._column
    @property
    def row_index(self):
        """
        Returns the row from 0 to n -1
        """
        return self._row_index

    def _set_column(self, col):
        """
        Sets the column. Private method.
        """
        if isinstance(col, str) or isinstance(col, bytes):
            pass
    
    @property
    def name(self):
        """
        Return the cell position in format "A1".
        """
        return self.column + str(self.row)
    
    @property
    def value(self):
        """
        It may be a synonym of text.
        """
        value = ''
        if self.segments:
            for seg in self.segments:
                value += seg.text
        else:
            value = self.text
        return value

    @property
    def segments(self):
        """
        """
        return self._segments

    @segments.setter
    def segments(self, value):
        if self.is_copy:
            self._segments = value

    @property
    def characters_count(self):
        """
        """
        if self._char_count == 0:
            self._char_count = len(self.xl_cell.value)
        return self._char_count

    @property
    def background_color(self):
        """
        """
        if self.xl_cell:
            pattern_color = self.workbook.workbook.colour_map[
                                     self.xl_format.background.pattern_colour_index]
            self._background_color = pattern_color
        else:
            pattern_color = self._background_color
        return pattern_color

    @background_color.setter
    def background_color(self, value):
        pass

    @property
    def background_color_hex(self):
        """
        """
        return rgb2hex(self.background_color)

    @property
    def background_fill_pattern(self):
        """
        """
        if self.xl_cell:
            fill_pattern = self.xl_format.background.fill_pattern
            self._background_pattern = fill_pattern
        else:
            fill_pattern = self._background_pattern
        return fill_pattern

    @property
    def notes(self):
        """
        """
        return self._notes

    @property
    def maxlen(self):
        return self._maxlen

    @maxlen.setter
    def maxlen(self, value):

        self._maxlen = int(value)

    def add_note(self, value):
        """
        Add a note to the cell.
        
        Arguments:
            value(str): Note to be added to the cell.
        """
        if value:
            self._notes.append(value)

    def remove_notes(self):
        """
        Removes all the notes. It creates an empty list.
        """
        self._notes = []

    def copy(self, target_row, target_column,
                  workbook=None,  worksheet=None, is_writable=False, copy_format=False):
        """
        Copies the cell to another cell.
        
        Arguments:
            target_row(int): Row number of the cell where the cell has to be copied. 
                                   From 0 to n-1.
            target_column(int): Column number of the cell where the cell has to be copied. 
                                        From 0 to n-1.
            workbook(ExcelFile): Workbook to where has to be copied the cell. Optional. 
                                        Otherwise is copied in the same workbook.
            worksheet(WorkSheet): Worksheet to where has to be copied.
            is_writable(bool): if it is set up, it indicates that the cell has to be stored in the new position.
            copy_format(bool): If it ise set up, the format is copied to the new position.
        """
        tgt_cell = self.__cell_copied(target_row, target_column)
        if not workbook and not worksheet:
            ws = self.worksheet
            wb = self.workbook

        if not workbook and worksheet:
            ws = worksheet
            wb = self.workbook    
        if workbook and worksheet:
            ws = worksheet
            wb = workbook
        tgt_cell.worksheet = ws
        tgt_cell.workbook = wb
        tgt_cell.is_writable = is_writable
        self._target_cells.append(tgt_cell)
        for col in ws._cells.values():
            for cell in col.values():
                if cell.row == tgt_cell.row and cell.column == tgt_cell.column:
                    cell.text = tgt_cell.text
                    cell.font = tgt_cell.font
                    cell.rich_text_list = tgt_cell.rich_text_list
                    cell.segments = tgt_cell.segments
        print( 'Printing the cell: ',
                  ws._cells[tgt_cell.row_index][tgt_cell.column_id - 1].text)
        ws._cells[tgt_cell.row_index][tgt_cell.column_id - 1] = tgt_cell

        return tgt_cell, wb, ws


    def __cell_copied(self, target_row, target_column, copy_format=False):
        """
        Internal method to define the target cell.
        """
        tgt_cell = Cell(target_row, target_column, copy=True)
        tgt_cell.text = self.text
        tgt_cell.notes = self.notes
        tgt_cell._source_cell = self
        if copy_format:
            tgt_cell.format = self.format
        else:
            if self.worksheet.cells(target_row + 1, target_column + 1):
                tgt_cell.format = self.worksheet.cells(target_row + 1, target_column + 1).format
            else:
                tgt_cell.format = Format(None, Font('Arial', 11, (0, 0, 0), False, False, False, 0), workbook=self.workbook)
        tgt_cell.font = self.font
        tgt_cell.rich_text_list = self.rich_text_list
        tgt_cell._segments = self.segments
        return tgt_cell

    def _get_value(self):
        """
        """
        segments=[]
        if self.text and self.rich_text_list:
            self.has_rich_text = True
            for segment_index in range(len(self.rich_text_list)):
                start = self.rich_text_list[segment_index][0]
                end = None
                if segment_index != len(self.rich_text_list) -1:
                    end = self.rich_text_list[segment_index + 1][0]
                segment_text = self.text[start:end]

                segment_obj = RichTextSegment(segment_text, start,
                                                     end,
                                                     self.workbook.workbook.font_list[
                                                              self.rich_text_list[segment_index][1]],
                                                     self.workbook, self)

                segment_obj.worksheet = self.worksheet
                #segment_obj.xl_cell = self.xl_cell
                segments.append(segment_obj)

            if self.rich_text_list[0][0] != 0:
                end = self.rich_text_list[0][0]

                segment_obj = RichTextSegment(
                                                          self.text[:self.rich_text_list[0][0]], 0,
                                                          end, self.xl_font, self.workbook, self)

                segment_obj.worksheet = self.worksheet
                #segment_obj.xl_cell = self.xl_cell
                segments.insert(0, segment_obj)
        return segments

class Range:
    """
    Define a range of cells between the indicated rows and cells.
    
    Arguments:
        start_row (int): Initial row where starts the range. Values from 0 to n-1.
        start_column (int): Initial column where starts the range. Values from 0 to n-1.
        end_row (int): Last row of the range. Values from 0 to n-1.
        end_column (int): Last column of the range. Values from 0 to n-1.
        workbook (ExcelFile): Workbook of the range.
        worksheet (WorkSheet): Worksheet of the range.
        name (str): Name of the range. It can be any string value.
    """
    def __init__(self, start_row, start_column, end_row, end_column,
                       workbook=None, worksheet=None, name=''):
        self.start_row = start_row
        self.start_column = start_column
        self.end_row = end_row
        self.end_column = end_column
        self.workbook = workbook
        self.worksheet = worksheet
        self._cells = []
        self.excluded_cells = []
        self.type = ''
        self.name = name
        
        if self.worksheet:
            self._add_cells()

    def _add_cells(self):
        """
        Internal/private method to add cells to the range.
        """        
        for cell in self.worksheet.cells():
            if (cell not in self.excluded_cells and self.start_row <= cell.row <= self.end_row
              and self.start_column <= cell.column_id -1  <= self.end_column):
                self._cells.append(cell)

    def cells(self, row=None, column=None, name_cell=''):
        """
        Method that allows to access to the cells of the range. It return all cells of the range or
        one cell.
            
        Examples:
            Example 1:
                workbook = ExcelFile('sample.xls')
                for ws in workbook.worksheets:
                    print (ws.range.cells()) # Print all the cells of the active range of each worksheet.
            
            Example 2:
                workbook = ExcelFile('sample.xls')
                workbook.worksheets[0].range.cells(4,1).text # Print the text of cell(4,1)
                
            Example 3:
                workbook = ExcelFile('sample.xls')
                for ws in workbook.worksheets:
                    print(ws.range.cell(name='A1')) # Print the first cell of each worksheet
                    
        Arguments:
            row (int): Row of the cell that we want to access. From 1 to n.
            column (int): Column of the cell that we want to access. From to n
            name_cell (str): The name of the cell in format "B1"

        Return:
            cells_list (list): If no position of a cell has been passed returns a list of all the cells.
            cell (Cell): Return a cell.
        """
        if not row and not column and not name_cell:
            return [cell for cell in self._cells]
        elif ((not row and column)
               or (row and not column)
               or (row or column and name_cell)):
            return -1
        elif isinstance(row, int) and isinstance(column, int) and row and column:
            for cell in self._cells:
                
                if cell.row == row and cell.column_id -1 == column:
                    return cell
        elif not row and not column and isinstance(name_cell, str):
            for cell in self._cells:
             
                cell_pos = cell.column + str(cell.row)
                if cell_pos == name_cell:
                    return cell
                    

        
class Row(Range):
    """
    """
    def __init__(self, row, start_column=0, end_column=0,
                      workbook=None, worksheet=None, name=''):
        """
        """
        super().__init__(row,
                      start_column, row, 
                      end_column,
                      workbook=workbook,
                      worksheet=worksheet,
                      name=name)
        self.row = row
        self.row_excel = row + 1

def column_letter(column_value):
    """
    Expected values from 1..n
    return:
        column_name (str): Returns the name of the Excel column
        -1 (int): If the number introduced is superior to the max number of Excel columns
    """
    n = column_value
    s = ''
    if column_value < 16384:
        while n > 0:

            c = (n - 1) % 26
            s = chr(c + 65) + s
            n = (n - c) // 26
        column_name = s
    else:
        return -1
    return column_name

def column_letter_to_number(column_letter):
    """
    Takes the letter column and returns the number
    
    return:
        column_number (int): Return the column number in format 1..n
        -1 (int): The parameter is not valid
    """
    
    num = 0
    if isinstance(column_letter, str):
        for c in column_letter:
            if c in string.ascii_letters:
                num = num *26 + (ord(c.upper()) - ord('A')) + 1
        return num

class Column(Range):
    """
    """
    def __init__(self, column, start_row=0, end_row=0,
                         workbook=None, worksheet=None):
        """
        Attributes:
            column (str): Name of the column (A,B..XA)
            column_id (int): Column number based on 0..n-1
            column_ex_number: Column number based on 1..n
        """
        super().__init__(start_row, column, end_row, column, 
                                 workbook, worksheet)
        self.column = column_letter(column +1)
        self.column_id = column
        self.column_ex_number = column + 1

class HeaderDefinition(Range):
    """
    It defines the header of an excel file. It is a Range subclass.
    NOTE: Not tested and to be improved. Avoid using it.
    """

    def __init__(self):
        """
        """
        self.key_words = ['master',
                             'item', 
                             'comment', 
                             'translation mark', 
                             'master claims', 
                             'claim title'
                             'copy element', 
                             'master copy', 
                             'comment for TP']
        self.__rows_index = [r for r in range(6)]
        self.source_column_word = ['master',
                                              'master copy']

class WorkSheet:
    """
    It has the contents of the worksheets of a workbook.
    This class should not be directly instantiated. To use it access it from the class ExcelFile.worksheets.
    
    Example:
        workbook = ExcelFile('excel_file.xls')
        print(workbook.worksheets[0].name)
    
    Class attributes:
        cell_cls (Cell): Reference to the Cell class.
        range_cls (Range): Reference to the Range class.
    
    Attributes:
        workbook (Workbook): Back rerference to the workbook
        xl_worksheet (xlrd.Worksheet): xlrd Worksheet content.
        end_row (int): Last row of the Worksheet.
        start_row (int): First row of the Worksheet with content.
        end_column (int): Last column of the Worksheet.
        first_column (int): First column of the Worksheet with content.
        num_rows (int): Total number of rows in use of the Worksheet.
        num_cols (int): Total number of columns in use in the Worksheet.
        xl_cells (dict): Dictionary of the xlrd.Cells of the xlrd.Worksheet.
        is_wirtable (bol): Indicates if the worksheet has to be written or not.
        source_column (int): Indicates which is the source column where are stored the contents to be processed.
        notes_range (Range): Range of the columns that containt the notes.
        target_range (Range): Range cells where are stored the target languages, and where should be 
                                    copied the source content.
        hidden (bol): Indicates if the worksheet is hidden or not.
        range (Range): Active range of the sheet. It includes all the used cells.
        ranges(list): Store the different ranges of the sheet. Custom or already defined.
    
    Properties:
        columns_count (int): Total of columns of the worksheet.
        rows_count (int): Total number of rows of the file.
    
    Methods:
        cells(): Method to access to all cells of the worksheet or to a cell.
        add_range(start_row, end_row, start_column, end_column, name): Adds a range to the sheet.
    
    Private methods:
        _process_cells(): Processes the cells of the sheet. Internally used.
    """
    cell_cls = Cell
    range_cls = Range
    def __init__(self, name, xl_worksheet = None, workbook=None, hidden_cells=False):
        """
        Initialization of the WorkSheet object.
        
        Arguments:
            name (str): Name of the worksheet.
            xl_worksheet (xlrd.Worksheet): xlrd.Worksheet object. Used to process the Worksheet.
            workbook (Workbook): Back reference to the parent workbook of the sheet.
            hidden_cells (bol): Indicate if hidden cells need to be processed or not. 
        """
        self.workbook= workbook
        
        self.name = name
        self.xl_worksheet = xl_worksheet
        self.end_row = 0
        self.start_row = 0
        self.end_column = 0
        self.start_column = 0
        self.num_rows= 0
        self.num_cols = 0
        self._cells = dict()
        self.xl_cells = dict()
        self.rows = dict()
        self.columns = dict()
        if self.xl_worksheet:
            self.num_rows= xl_worksheet.ncols
            self.num_cols = xl_worksheet.nrows
            self._process_sheet_cells(self, self.num_rows, self.num_cols, hidden_cells)
        self.is_writable = False
        self.source_column = None
        self.notes_range = None
        self.target_range = None
        self.is_translatable = True
        self.non_translatable_cells = []
        self.hidden = False
        self.range = self.range_cls(0,
                                             0,
                                             self.xl_worksheet.nrows,
                                             self.xl_worksheet.ncols,
                                             self.workbook, self,
                                             'active_range')
        self.ranges = []
        self.ranges.append(self.range)

    def __str__(self):
        """
        """
        return f'{self.__class__.__name__} - {self.cell_cls.__name__}'

    @property
    def rows_count(self):
        """
        Total number of rows of the sheet.
        """
        if self.xl_worksheet:
            return self.xl_worksheet.nrows

    @property
    def columns_count(self):
        """
        Number of columns in the sheet that are used.
        """
        if self.xl_worksheet:
            return self.xl_worksheet.ncols

    def get_header(self):
        """
        Not implemented
        """
        pass

    def copy_column(self, src_col, tgt_col, start_row=0, 
                               ignore_interior_color=None, only_empty=True):
        """
        Copies the indicated column
        Not implemented.
        """
        pass

    def _process_sheet_cells(self, sheet, num_rows, num_cols, hidden_cells):
        """
        Private method that reads all the cells in the sheet and are stored in memory and in the 
        attribute WorkSheet._cells.
        """
        # Can this method be simplified. It looks too long. It should be shorten.
        xl_cells = dict()
        row_info = self.xl_worksheet.rowinfo_map
        col_info = self.xl_worksheet.colinfo_map
        self.end_row = self.xl_worksheet.nrows
        for row_index in range(self.end_row):
            print(row_index)
            row_cells = dict()
            print(self.xl_worksheet.row_len(row_index))
            for col_index in range(self.xl_worksheet.row_len(row_index) -1):
                if hidden_cells:
                    if not row_index in xl_cells:
                        xl_cells[row_index] = row_cells
                    cell = self.__class__.cell_cls(row_index, 
                                   col_index, self.xl_worksheet.cell(
                                                                row_index, col_index), self.workbook, self)
                    #cell.format = self.workbook.xf_list[self.cell_xf_index(row_index, col_index)]
                    if row_info[row_index].hidden == 1 or col_info[col_index].hidden == 1:
                        cell.hidden = True

                    cell.xl_font = self.workbook.workbook.font_list[cell.xl_format.font_index]
                    cell.workbook = self.workbook
                    cell.workheet = self
                    cell._background_color = self.workbook.workbook.colour_map[
                                     cell.xl_format.background.pattern_colour_index]
                    cell._background_pattern = cell.xl_format.background.fill_pattern
                    row_cells[col_index] = cell
                    # Check this Dictionary creation and may be out of the 2nd for and be only in the rows
                    xl_cells[row_index] = row_cells
                else:
                    try:
                        if row_info[row_index].hidden == 0:
                            if col_info[col_index].hidden == 0:                
                                if not row_index in xl_cells:                            
                                    xl_cells[row_index] = row_cells
                                self.xl_worksheet.cell(row_index, col_index)
                                cell = self.__class__.cell_cls(row_index, col_index, 
                                               self.xl_worksheet.cell(row_index, col_index),
                                               self.workbook, self)

                                cell.xl_font = self.workbook.workbook.font_list[
                                             cell.xl_format.font_index]
                                cell.workbook = self.workbook
                                cell.workself = self

                                row_cells[col_index] = cell
                                # Check this Dictionary creation and may be out of the 2nd for and be only in the rows
                                xl_cells[row_index] = row_cells 
                    except KeyError:
                        logging.critical('ExcelFile._process_self_cells: '
                                            'Sheet {0}, row index: {1}'.format(
                                                                self.name, row_index
                                                        ) +
                                             ',col index {}'.format(col_index))
        if xl_cells:
            self._cells = xl_cells
        #return xl_cells

    def add_range(self, start_row, end_row, start_column, end_column, name=None):
        """
        Add a range to the worksheet.
        Arguments:
            start_row:
        """
        rng = self.__class__.range(start_row,
                                             end_row,
                                             start_column,
                                             end_column,
                                             worksheet=self,
                                             name=name)
        self.ranges.append(rng)

    def column_letter(self, column_value):
        """
        Expected values from 1..n
        return:
            column_name (str): Returns the name of the Excel column
            -1 (int): The column_value is 0. It is not a valid number.
            -2 (int): If the number introduced is superior
                        to the max number of Excel columns
        """
        n = column_value
        s = ''
        if column_value == 0:
            return -1
        if column_value < 16384:
            while n > 0:

                c = (n - 1) % 26
                s = chr(c + 65) + s
                n = (n - c) // 26
            column_name = s
        else:
            return -2
        return column_name
    
    def column_letter_to_number(column_letter):
        """
        Takes the letter column and returns the number
        
        return:
            column_number (int): Return the column number in format 1..n
            -1 (int): The parameter is not valid
        """
        
        num = 0
        if isinstance(column_letter, string):
            for c in column_letter:
                if c in string.ascii_letters:
                    num = num *26 + (ord(c.upper()) - ord('A')) + 1
            return num
    
    def cells(self, row=None, column=None, name_cell=''):
        """
        Method that allows to access to the cells of the worksheet. It return all cells of the range or
        one cell.
            
        Examples:
            Example 1:
                workbook = ExcelFile('sample.xls')
                for ws in workbook.worksheets:
                    print (ws.cells()) # Print all the cells of the active range of each worksheet.
            
            Example 2:
                workbook = ExcelFile('sample.xls')
                workbook.worksheets[0].cells(4,1).text # Print the text of cell(4,1)
                
            Example 3:
                workbook = ExcelFile('sample.xls')
                for ws in workbook.worksheets:
                    print(ws.cells(name='A1')) # Print the first cell of each worksheet
                    
        Arguments:
            row (int): Row of the cell that we want to access. From 1 to n.
            column (int): Column of the cell that we want to access. From to n
            name_cell (str): The name of the cell in format "B1"
            
        Return:
            cells_list (list): If no position of a cell has been passed returns a list of all the cells.
            cell (Cell): Return a cell.
        """
        if not row and not column and not name_cell:
            return [cell for row in self._cells.values()
                       for cell in row.values()]
        #elif (not row and column) or (row and not column) or (row or column and name_cell):
          #  return -1
        elif (isinstance(row, int)
              and isinstance(column, int)
              and row
              and column):
            for rows in self._cells.values():
                for cell in rows.values():
                    if cell.row == row and cell.column_id -1 == column:
                        return cell
        elif not row and not column and isinstance(name_cell, str):
            for rows in self._cells.values():
                for cell in rows.values():
                    cell_pos = cell.column + str(cell.row)

                    if cell_pos == name_cell:
                        return cell

class ExcelFile:
    """
    Allow to read an Excel file. The Excel can be loaded when creating the object. Or later.
    All the contents of the Excel are loaded in the memory if it is passed.
    
    Examples:
        Example 1:
            excel = ExcelFile('c:\excel.xls')
            # Loads the Excel File and reads its contents
        Example 2:
            excel = ExcelFile()
            excel.open('c:\excel.xls')
            excel.read_workbook()
            #The same as with Example 1, but doing it separately
    
    Attributes:
        read_mode (bol): If the Excel only can be read or not. By default is read-only.
        excel_file (str): Path string to the Excel file.
        excel_file_path (Path): Path object to the Excel file.
        excel_type (str): Type of the excel. It is supported only (xls,xlsx, xlsm, xlt and xltm)
        workbook (xlrd.Workbook): Loads the workbook of the internal library that is used to process the Excel files.
        worksheets (list): List of worksheets (WorkSheet) of the Workbook(ExcelFile).
    
    Properties:
        name (str): Name of the Excel file without extension.
        full_name (str): Name of the Excel fiel with extension.
        excel_extensions (tuple): List the supported Excel extensions.
    
    Methods:
        open(excel_file, ragged_rows, read_mode): Open the Excel file.
        read_workbook(hidden_sheet, hidden_cells): Read the content of the opened workbook.
        save(file_name, save_as): Save the changes done to the Excel file.
    """
    _excel_extensions = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')
    sheet_cls = WorkSheet
    
    def __init__(self, excel_file=None, read_mode=True):
        """
        Initialize the object.
        
        Example:
            workbook = ExcelFile(excel_file='c:\excel.xlsx', read_mode=True)
            
        Arguments:
            excel_file (str): String path to the file
            read_mode (bol): Indicates if the Excel is only readable or it also can be written on it.
        """
        self.read_mode = read_mode
        self.excel_file = ''
        self.excel_file_path = None
        self.excel_type = ''
        self.workbook = None
        self.worksheets = []
        if excel_file:
            if self.read_mode:
                self.excel_file_path = Path(excel_file)
                self.excel_file = str(self.excel_file_path)
                self.excel_type = str(self.excel_file_path.suffix).replace('.', '')
                self.open(self.excel_file, True)
        if self.workbook:
            self.sheet_names = self.workbook.sheet_names()
            if self.read_mode:
                self.read_workbook()

    @property
    def excel_extensions(self):
        """
        Return the supported excel extensions.
        """
        return ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')

    @property
    def name(self):
        """
        Get the name of the Excel file.
        """
        if self.excel_file_path:
            return self.excel_file_path.stem

    @property
    def full_name(self):
        """
        Get the name of the Excel file with the extension included.
        """
        if self.excel_file_path:
            return self.excel_file_path.name

    @property
    def worksheets_count(self):
        """
        Number of worksheets of the Workbook
        """
        return len(self.worksheets)

    def open(self, excel_file, ragged_rows=False, read_mode=True):
        """
        Open the indicated Excel file.
        
        Example:
            workbook = ExcelFile()
            workbook.open('excel.xlsx', ragged_rows=True, read_mode=True)
        
        Arguments:
            excel_file(str): String path to the Excel file to open.
            ragged_rows(bol): xlrd.Workbook option to allow to limit the size of the rows. Default: False
            read_mode(bol): Open the file in read mode. Default: True
        
        Return:
            Successful:
                workbook(xlrd.Workbook): Return the xlrd.Workbook of the opened Excel file.
            Failure:
                -1: If the file does not exist or could not be found.
        """
        self.read_mode = read_mode
        self.excel_file_path = Path(excel_file)
        self.excel_file_str = str(self.excel_file_path)
        if (self.excel_file_path.exists()
             and self.excel_file_path.suffix in self.excel_extensions):
            self.excel_type = self.excel_file_path.suffix
            self.excel_type = self.excel_type.replace('.', '')
            if self.excel_type != 'xls':
                excel_app = Dispatch('Excel.Application')
                excel_app.Visible = False
                excel_app.DisplayAlerts = False
                workbook = excel_app.Workbooks.Open(self.excel_file_str)

                xls_path = Path(self.excel_file_path.parent, 'tmp_xlrd.xls')
                xls = str(xls_path)

                file_format = XlFileFormat.xls

                workbook.SaveAs(xls, FileFormat=file_format.value)
                workbook.Close(SaveChanges=False)
                excel_app.DisplayAlerts = True
                excel_app.Quit()
                self.temp_xls_path = system_tools.delete_file(xls)
                self.temp_xls = str(self.temp_xls_path)
                self.workbook = xlrd.open_workbook(self.temp_xls,
                                                                  formatting_info = True,
                                                                  ragged_rows=ragged_rows)
            else:
                self.workbook = xlrd.open_workbook(self.excel_file_str,
                                                                 formatting_info = True,
                                                                 ragged_rows=ragged_rows)
            #system_tools.delete_file_permanently(self.temp_xls)
            return self.workbook
        else:
            return -1

    def save(self, file_name, save_as):
        """
        Save the Excel file
        """
        self.workbook.save(FileName=file_name, SaveAs=save_as)

    def read_workbook(self, hidden_sheet=False, hidden_cells=False):
        """
        Read the workbook. It reads all the worksheets and its cells and stores all its content in memory.
        
        Arguments:
            hidden_sheet (bol): Indicate if the hidden sheet need to be processed. Default: False. 
                            By default the hidden sheets are ignored and they are not processed.
            hidden_cells (bol): Flago to indicate if the hidden cells are in scope 
        """
        self.worksheets = []
        for sheet in self.workbook.sheets():

            if hidden_sheet:
                #worksheet = WorkSheet(sheet.name)
                worksheet = self.__class__.sheet_cls(sheet.name, sheet, self, hidden_cells)
                worksheet.num_rows = sheet.nrows
                worksheet.num_cols = sheet.ncols
                #worksheet.xl_worksheet = sheet
                worksheet.workbook = self
                worksheet._process_sheet_cells(
                                                             worksheet,
                                                             worksheet.num_rows,
                                                             worksheet.num_cols,
                                                             hidden_cells)
                if sheet.visibility != 0:
                    worksheet.hidden = True

                self.worksheets.append(worksheet)
            else:

                if sheet.visibility == 0:
                    #worksheet = WorkSheet(sheet.name)
                    
                    worksheet = self.__class__.sheet_cls(sheet.name, sheet, self, hidden_cells)
                    worksheet.num_rows = sheet.nrows
                    worksheet.num_cols = sheet.ncols
                    #worksheet.xl_worksheet = sheet
                    worksheet.workbook = self
                    worksheet._process_sheet_cells(
                                                                 worksheet,
                                                                 worksheet.num_rows,
                                                                 worksheet.num_cols,
                                                                 hidden_cells)
                    self.worksheets.append(worksheet)

if __name__ == '__main__':
    excel = ExcelFile(r"c:\Programming\Python\multilingual_excel\PPV_Omega_Conditioner_Europe_translation.xls")
    #excel = ExcelFile(r"c:\Programming\Python\multilingual_excel\files\ArtWorks_Pampers\MCCLCC_DO_Pure with CU v1 5-15-2018.xlsm")
    print(len(excel.worksheets))
    for worksheet in excel.worksheets:
        print(worksheet.num_cols)
        for col in worksheet._cells.values():
            for cell in col.values():
                print(cell.text)
                print(cell.background_color)
                print(cell.rich_text_list)
                if cell.rich_text_list:
                    print(cell.segments)
                    print(cell.segments[0].format.font.bold)
    try:
        print(excel.worksheets[0]._cells[5][2].text)
        print(excel.worksheets[0]._cells[5][2].rich_text_list)
        print(excel.worksheets[0]._cells[5][2].format.font.bold)
        print(excel.worksheets[0]._cells[5][4].background_color)
        print(excel.worksheets[0]._cells[5][2]._get_value())

        print(excel.worksheets[0]._cells[5][2].characters_count)


        #print(excel.worksheets[0]._cells[5][2]._target_cells[0].segments[0].font.bold)
        print(excel.worksheets[0]._cells[0][1].value)
        print(excel.worksheets[0].xl_worksheet.merged_cells)
        print(excel.worksheets[0].name)
        print(dir(excel.worksheets[0]._cells[5][4].format.xl_format.border))
        print(excel.worksheets[0]._cells[5][4].text)
        print(excel.worksheets[0]._cells[5][4].format.border.line_style.value)
        print(excel.worksheets[0]._cells[5][4].format.border.color)
        print(excel.worksheets[0].range.end_row)
        
    except KeyError:
        pass
    for worksheet in excel.worksheets:
        for row in worksheet.cells():
            print(cell)
        print (worksheet.cells(name_cell='C3').text)
        print(worksheet.num_cols)
    tgt_cell, excel, excel.worksheets[0] = excel.worksheets[0]._cells[5][2].copy(5, 4)
    excel.worksheets[0].cells(name_cell='C7').copy(6, 4)
    print('Source cell1: ', excel.worksheets[0].cells(name_cell='C6').text)
    print('Copied cell 1: ',  worksheet.cells(name_cell='E6').text)
    print('Copied cell 2: ', worksheet.cells(name_cell='E7').text)
    print(excel.worksheets[0].ranges[0].cells())
    rng = Range(4, 2, 20, 2, excel, excel.worksheets[0], 'test')
    print(rng.cells())
    print(rng.name)
