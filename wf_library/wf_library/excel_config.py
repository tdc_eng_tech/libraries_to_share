"""
Generate Excel Config files.
Its main class is ExcelConfig.
"""
__author__ = 'Llorenç Suau'
__copyright__ = 'Copyright 2018 (c) All rights are reserved'
__license__ = 'LGPL 3'

import re
from lxml import etree as et
#from pathlib import Path

class Sheet:
    """
    Definition of the sheet of an ExcelConfig
    """
    def __init__(self):
        """
        Construct and initialize a Sheet object.
        
        Properties:
            name (str): Name of the sheet. Incompatible with number_sheet.
            number_sheet (int): Number of the sheet in the Excel. Incompatible with name.
            start_row (int): First row to be parsed. 0-Int based number.
            end_row (int): Last row to be parsed. 0-Int based number.
        """
        self._sheet_name = None
        self._number_sheet = None # Number of the sheet from 0 to n-1
        self._start_row = 0
        self._end_row = None
        self._source_column_range = ''
        self._source_column = None
        self._target_column = None
        self._maxlen_column = None
        self._attributes_columns = dict() # From PD15, name of attribute and column
        self._columns = []
    
    @property
    def name(self):
        """
        Name of the worksheet. It is not compatible with number_sheet property.
        Only name or number_sheet can be set up.
        
        Example:
            sheet.name = 'Test' # Set the sheet name as 'Test'
            sheet.name return the name of the sheet.
            
        """
        if self._sheet_name:
            return self._sheet_name
    
    @name.setter
    def name(self, value):
        
        pat = re.compile('\w')
        try:
            pat.search(value)
        except TypeError:
            return -1
        if not self._number_sheet:
            self._sheet_name = value
        else:
            self._number_sheet = None
            self._sheet_name = value
    
    @property
    def number_sheet(self):
        """
        It is used to set the number of sheet to be added to the config.
        it is not compatible with name. Only one of them can be used.
        """
        if self._number_sheet:
            return self._number_sheet
    
    @number_sheet.setter
    def number_sheet(self, value):
        pat = re.compile('^\d+?$')
        if pat.search(str(value)):
            self._number_sheet = value
        if not self._sheet_name:
            self._sheet_name  = ''
    
    @property
    def source_column_range(self):
        """
        Return the range of source columns.
        """
        if self._source_column_range:
            return self._source_column_range
        else:
            return None
    
    def set_source_column_range(self, value):
        """
        Set the source column range.
        
        Arguments:
            value (str): Range of columns in scope.
        """
        # Check '^([0-9]{1,5}(-[0-9]{1,5})?)(;([0-9]{1,5}-[0-9]{1,5}|[0-9]{1,5}))?$'. 
        # Check if the value is a valid Excel range.
        # The value should be something like "0-1;5;7-9"
        # The Regex returnd None if there is anything else.
        
        pat_syntax_range = re.compile('^([0-9]{1,5}(-[0-9]{1,5})*)'
                                     '(;([0-9]{1,5}-[0-9]{1,5}|[0-9]{1,5}))*$')
        if pat_syntax_range.match(value) and not self.columns:
            self._source_column_range = value
        else:
            return -1
        return 0
    
    @property
    def start_row(self):
        """
        Indicate the initial row to be parsed.
        """
        if self._start_row:
            return str(self._start_row)
    
    @start_row.setter
    def start_row(self, value):
        if re.match('[0-9]{1,9}', str(value)) and int(value)>=0:
            self._start_row = str(value)
    
    @property
    def end_row(self):
        """
        Indicate the last row to be parsed
        """
        if self._end_row:
            return str(self._end_row)
    
    @end_row.setter
    def end_row(self, value):
        """
        """
        if re.match('[0-9]{1,9}', str(value)) and int(value)>=0:
            self._end_row = str(value)
    
    @property
    def columns(self):
        """
        It returns the columns that are in scope
        """
        return self._columns

    @property
    def attributes_columns(self):
        """
        Only read property to get the attributes columns
        """
        return self._attributes_columns

    def set_attribute_column(self, attribute_name: str, column: int) -> int:
        """
        Sets the column and attribute name containing the different attributes.
        
        Arguments:
            attribute_name (str): The name of the attribute to be added.
            column (int): The column value in base 0 values.
        
        Return:
            0: If is successful
            1: If one of the values is not valid
        """
        if isinstance(attribute_name, str) and isinstance(column, int):
            self._attributes_columns[attribute_name] = column
            return 0
        else:
            return 1
            
    
    def add_column(self, source, target=None, maxlen=None, attributes: dict={}):
        """
        Add a column to the columns list
        Arguments:
            source (int): Integer value of the source column. Starting 0-number.
            target  (int): Integer value of the traget column. 0-number format.
            maxlen (int): Integer value of the length restriction column. 0-number format
            attributes (dict): Contains the column in 0-number integer format and the name for these attributes.
        
        Example:
            self.add_column(3, 4, 2, {'Key': 0})
        """
        if not self.source_column_range:
            column =dict()
            if int(source) >= 0:
                column['source'] = int(source)
                
            if int(target) >= 0:
                column['target'] = int(target)
            elif not target:
                column['target'] = column['source']
            if int(maxlen) > -1:
                column['maxlen'] = int(maxlen)
            else:
                column['maxlen'] = None
            if attributes:
                column['attributes'] = dict()
                for attr, col in attributes.items():
                    column['attributes'][attr] = col
            self._columns.append(column)

        else:
            return self.source_column_range 
        return 0


class ExcelConfig:
    """
    Create an Excel Config.
    """
    def __init__(self, ex_conf_path=''):
        """
        Initialize the class
        """
        self._ex_conf_path = ex_conf_path
        self._regexp = ''
        self.trans_sheet_names = 'false'
        self.formula_cells = 'false'
        self.hidden_cells = 'false'
        self.hidden_sheets = 'false'
        self.cell_comments = 'false'
        self.data_validation_comments = 'false'
        self.drop_down_list = 'true'
        self.number_extraction_on_text = 'false'
        self.column_order = 'true'
        self.break_strategy = 'break'
        self.shapes = 'true' # New from Filters 5.5
        self.charts = 'true' # New from Filters 5.7
        self._sheets = []
#        if self._ex_conf_path and Path(self._ex_conf_path).exists():
#            self.__parse_config()
    
    @property
    def regexp(self):
        """
        Adds a regexp to filter the contents out of scope for the converter
        convert these contents as internal tags.
        """
        return self._regexp
    
    @regexp.setter
    def regexp(self, value):
        if isinstance(value, str):
            try:
                re.compile(value)
            except re.error:
                return -1
            self._regexp = value
    
    @property
    def sheets(self):
        """
        Return all the sheets contained in the config
        """
        return self._sheets
    
    def add_sheet(self, sheet: Sheet):
        """
        Adds a sheet to the Excel Config
        """
        if isinstance(sheet, Sheet):
            self._sheets.append(sheet)
            return sheet
        else:
            return -1

    def __parse_config(self):
        """
        Read an existing config and convert it to an object.
        It is an experimental function, use with caution.
        """
        if self._ex_conf_path:
            tree = et.parse(str(self._ex_conf_path))
            for elem in tree.iter():
                if elem.tag == 'regexp':
                    if elem.text:
                        self.regexp = elem.text
                elif elem.tag == 'translate_sheet_names':
                    self.trans_sheet_names = elem.text
                elif elem.tag == 'translate_formula_cells':
                    self.formula_cells = elem.text
                elif elem.tag == 'translate_hidden_cells':
                    self.hidden_cells = elem.text
                elif elem.tag == 'translate_hidden_sheets':
                    self.hidden_sheets = elem.text
                elif elem.tag == 'translate_cell_comments':
                    self.cell_comments = elem.text
                elif elem.tag == 'translate_data_validation_comments':
                    self.data_validation_comments = elem.text
                elif elem.tag == 'translate_drop_down_lists':
                    self.drop_down_list = elem.text
                elif elem.tag == 'number_extraction_on_text_cells':
                    self.number_extraction_on_text = elem.text
                elif elem.tag == 'extract_cell_order_by_column':
                    self.column_order = elem.text
                elif elem.tag == 'sheets':
                    self.__parse_sheets(elem)

    def __parse_sheets(self, sheets: et.Element):
        """
        Parse the sheets elements
        
        Arguments:
            sheets: Sheet elements extracted from the main parser
        """
        for sh in sheets.findall('sheet'):
            sheet = Sheet()
            for elem in sh.iter():
                sheet.name = elem.text if elem.tag == 'name' and elem.text else None
                sheet.number_sheet = elem.text if elem.tag == 'number' and elem.text else None
                if elem.tag == 'columns':
                    sheet = self.__parse_columns(elem, sheet)
            self.add_sheet(sheet)

    def __parse_columns(self, columns: et.Element, sheet: Sheet):
        """
        Parse the columns section of an existing ExcelConfig.
        """
        if columns.find('column') is not None:
            for col in columns.findall('column'):
                attributes = {}
                for elem in col.iter():
                    source = elem.text if elem.tag == 'sourceColumn' and elem.text else None
                    target = elem.text if elem.tag == 'targetColumn' and elem.text else None
                    maxlen = elem.text if elem.tag == 'lengthrestricctionColumn' and elem.text else None
                    if elem.tag == 'customAttributeColumn':
                        attr_name = elem.get('name')
                        attr_column = str(elem.text)
                        attributes[attr_name] = attr_column
                sheet.add_column(source, target, maxlen, attributes )
        if columns.find('sourceColumnRange') is not None:
            sheet.set_source_column_range(columns.find('sourceColumnRange').text)
        return sheet

    def write_config(self, save_to: str=''):
        """
        Save the configuration into an XML excelConfig file.
        Arguments:
            save_to (str): Path and name of the config where be saved.
        """
        # configuration as Root element of the XML.
        configuration = et.Element('configuration')
        # Generate Element Tree of Configuration
        config = et.ElementTree(configuration)
        if self.regexp:
            regexp = et.SubElement(configuration, 'regexp')
            regexp.text = self.regexp
        translate_sheet_names = et.SubElement(configuration, 'translate_sheet_names')
        translate_sheet_names.text = self.trans_sheet_names
        formula_cells = et.SubElement(configuration, 'translate_formula_cells')
        formula_cells.text = self.formula_cells
        hidden_cells = et.SubElement(configuration, 'translate_hidden_cells')
        hidden_cells.text = self.hidden_cells
        hidden_sheets = et.SubElement(configuration, 'translate_hidden_sheets')
        hidden_sheets.text = self.hidden_sheets
        cell_comments = et.SubElement(configuration, 'translate_cell_comments')
        cell_comments.text = self.cell_comments
        data_validation_comments = et.SubElement(configuration, 'translate_data_validation_comments')
        data_validation_comments.text = self.data_validation_comments
        drop_down_list = et.SubElement(configuration, 'translate_drop_down_lists')
        drop_down_list.text = self.drop_down_list
        number_extraction_on_text = et.SubElement(configuration, 'number_extraction_on_text_cells')
        number_extraction_on_text.text = self.number_extraction_on_text
        shapes_content = et.SubElement(configuration, 'extract_shapes_content')
        shapes_content.text = self.shapes
        charts_content = et.SubElement(configuration, 'extract_charts_content')
        charts_content.text  = self.charts
        extract_soft_return_strategy = et.SubElement(configuration, 'extract_soft_return_strategy')
        extract_soft_return_strategy.text = self.break_strategy
        column_order = et.SubElement(configuration, 'extract_cell_order_by_column')
        column_order.text = self.column_order       
        if self.sheets:
            sheets = et.SubElement(configuration, 'sheets')
            sheets.set('{http://www.w3.org/XML/1998/namespace}space', 'preserve')
            
            for sheet in self.sheets:
                xml_sheet = et.SubElement(sheets, 'sheet')
                if sheet.name and not sheet.number_sheet:
                    sheet_name = et.SubElement(xml_sheet, 'name')
                    sheet_name.text = sheet.name
                elif sheet.number_sheet:
                    number_sheet = et.SubElement(xml_sheet, 'number')
                    number_sheet.text = str(sheet.number_sheet)
                if sheet.start_row:
                    start_row = et.SubElement(xml_sheet, 'rowFromNumber')
                    start_row.text = str(sheet.start_row)
                if sheet.end_row:
                    end_row = et.SubElement(xml_sheet, 'rowEndNumber')
                    end_row.text = str(sheet.end_row)
                if sheet.columns and not sheet.source_column_range:
                    columns = et.SubElement(xml_sheet, 'columns')
                    for column in sheet.columns:

                        if column['source'] >= 0:

                            column_xml = et.SubElement(columns, 'column')
                            source = et.SubElement(column_xml, 'sourceColumn')                            
                            source_str = str(column['source'])
                            source.text = source_str
                            if column['target'] >= 0:
                                target = et.SubElement(column_xml, 'targetColumn')
                                target.text = str(column['target'])
                            if column['maxlen']:
                                maxlen = et.SubElement(column_xml, 'lengthRestrictionColumn')
                                maxlen.text = str(column['maxlen'])
                            if column.get('attributes'):
                                for attr, col in column['attributes'].items():
                                    print(attr, col)
                                    attribute_column = et.SubElement(column_xml, 'customAttributeColumn')
                                    attribute_column.set('name', attr)
                                    attribute_column.text = col
                if not sheet.columns and sheet.source_column_range:
                    columns = et.SubElement(xml_sheet, 'columns')
                    source_column_range = et.SubElement(columns, 'sourceColumnRange')
                    source_column_range.text = str(sheet.source_column_range)
        if save_to:
            # Save the XML.
            print(save_to)
            config.write(str(save_to), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True )
        else:
            if self.ex_conf_path:
                # Save the XML.
                print(self.ex_conf_path)
                config.write(str(self.ex_conf_path), encoding='utf-8', pretty_print=True, 
                 xml_declaration=True )


if __name__ == '__main__':
    config_file = ExcelConfig()
    sheet = Sheet()
    sheet.set_source_column_range('0-3;5-8;9;20-22')
    sheet.start_row = 3
    sheet.name = 'Master1'
    config_file.add_sheet(sheet)
    sheet = Sheet()
    sheet.add_column(0, 4, 5, {'key': '3'})
    sheet.add_column(6, 7, 8)
    print(sheet.columns)
    sheet.name = 'RegionalMaster2'
    sheet.start_row = '2'
    print(sheet.start_row)
    sheet.end_row = '31'
    config_file.add_sheet(sheet)
    config_file.write_config(r'q:\zz_Temp\lsuau\Development\Python\wf_library\files\excelconfig\Config.xml')
