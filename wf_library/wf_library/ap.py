"""
Interface with ap. Function module.
"""
__author__ = 'Llorenç Suau'
__copyright__ = 'Copyright 2018 (c) All rights are reserved'
__license__ = 'LGPL 3'
import logging
from wf_library.xml_wf import Wf, WfGroup
from pathlib import Path
from subprocess import Popen, run
import platform

_no_console = 0x08000000

AP2_BIN_LOCAL = r'c:\Program Files (x86)\Analysis Package\bin'
AP4_BIN_LOCAL = r'c:\Program Files (x86)\Analysis Package\AP4\bin'
AP2_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP2.20\bin'
AP4_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP4.18\bin'
AP5_BIN_REMOTE = r'\\bcnal-share\public\production\engineering\Admin_Engineering\_Scripts\_Controlled_Versions\AP5_2018_04\bin'

def run_command(command, threaded, use_shell, show_console, path_str=''):
    """
    Run an external command through subprocess.
    """
    path = Path(path_str)
    if path.exists() and path.is_dir():
        if threaded:
            sh_command = Popen(command,  shell=False, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
        else:
            sh_command = Popen(command,  shell=False, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
            sh_command.wait()
    else:
        if threaded:
            sh_command = Popen(command,  shell=False, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
        else:
            sh_command = Popen(command,  shell=False, 
                                            cwd=str(path_str), 
                                            creationflags=_no_console)
            sh_command.wait()
    return sh_command

            
def _add_wf_files_to_group(files_path, ap_version):
    """
    Add the generated wf objects to a WfGroup.
    
    Arguments:
        files_path (str): Path to the files to add to the group
        ap_version (str): Version of AP to be used (txlf/txml).
    """
    ap_version = ap_version.replace('.', '')
    wf_group = WfGroup()
    if Path(files_path).is_dir():
        for file in files_path.glob('**/*.{}'.format(ap_version)):
            file_to_process = str(file)
            wf_file = Wf(file_to_process)
            wf_group.append(wf_file)
    if Path(files_path).is_file():
        if str(Path(files_path).suffix).replace('.', '') == ap_version:        
            wf_file = Wf(str(files_path))
            wf_group.append(wf_file)
        else:
            parent_folder = Path(Path(files_path).parent)

            extension = '*.{}'.format(ap_version)
            for file in parent_folder.glob(extension):
                file_to_process = str(file)
                print(f'adding the {file_to_process} to the group')
                if file.stem == Path(files_path).name:
                    wf_file = Wf(file_to_process)
                    wf_group.append(wf_file)
    return wf_group

class Converter:
    """
    Converter class to convert any kind of file as most transparent way possible.
    
    Class Properties:
        excel_file_ext (tuple): Tuple of accepted extensions for Excel files.
        doc_file_ext (tuple): Tuple of accepted extensions for Word files.
        xml_file_ext (tuple): Tuple of accepted extensions for xml files.
        ppt_file_ext (tuple): Tuple of accepted extensions for PPT files.
        xlf_file_ext (tuple): Tuple of accepted extensionf for XLIFF format files.
        txt_file_ext (tuple): Accepted extensions for generic text files (json, yml and txt)
    
    Properties:
        ap_version (str): AP version that needs to be loaded (txlf, txml)
        src_lang (str): ISO code of the source language. Default: en
        tgt_lang (str): ISO code of the target language. Default: de
        ap_path (Path): Path to the ap commands.
        os (str): Indicate in what os is running the commands. It can be 'Linux', 'Windows' or 'MacOS'.
    """
    excel_file_ext = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')
    doc_file_ext = ('.doc', '.docx', '.docm', '.dot',
                      '.dotm', '.rtf')
    xml_file_ext = ('.xml', '.dita', '.tag')
    ppt_file_ext = ('.ppt', '.pptx', '.pptm')
    xlf_file_ext = ('.xlf', '.xliff', '.sdlxliff')
    txt_file_ext = ('.json', 'yml', '.txt')
    html_file_ext = ('.html', '.htm', '.chm')
    office_files_ext =('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm',
                            '.doc', '.docx', '.docm', '.dot', '.dotm', '.rtf', 
                            '.ppt', '.pptx', '.pptm')

    def __init__(self, ap_path, ap_version):
        self.ap_version = ap_version
        self.src_lang = 'en'
        self.tgt_lang = 'de'
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path_str, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()

    def convert_pdf(self, files_path_str: str, lang_iso: str, threaded: bool=False):
        """
        Convert PDF files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertPDF.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertPDF.ksh')
        command_str = str(command)
        #doc_types = ('.doc', '.docx', '.docm', '.dot', '.dotm')
        files_str = str(Path(files_path_str))
        if not self.ap_version:
            return -1
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return -1
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', lang_iso] 
            command_run = Popen(full_command, creationflags=_no_console)
            command_run.wait()
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)

    def convert_doc(self, files_path_str: str, lang_iso: str, threaded: bool=False):
        """
        Convert Word files into TXML/TXLF files.
        
        Arguments:
            files_path_str (str): Path to the files to convert
            lang_iso (str): Source language of the files.
            threaded (bool): Indicate if the executions of the command has to be threaded or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertDOC.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertDOC.ksh')
        command_str = str(command)
        #doc_types = ('.doc', '.docx', '.docm', '.dot', '.dotm')
        files_str = str(Path(files_path_str))
        if not self.ap_version:
            return -1
        ap_exists = False
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            return -1
        if ap_exists and not threaded:
            full_command = [command_str, str(files_str), '-l', lang_iso] 
            command_run = Popen(full_command, creationflags=_no_console)
            command_run.wait()
        elif threaded:
            raise NotImplementedError
        return _add_wf_files_to_group(files_path_str, self.ap_version)
  
    def convert_excel_to_wf(self, 
                        files_path_str,
                        excel_config_path,
                        lang_iso,
                        threaded=False, 
                        show_console=False):
        """
        Convert a file or folder to txml or txlf.
        convert_excel_to_wf (parameters) return error_code
        
        Arguments:
        files_path_str: String that indicates the file or folder to be converted.
                         It needs to be an Excel file.
        excel_config_path: XML Configuration file for the conversion of the actual file or files.
        lang_iso: source language of the file 
        ap_path_str: Path to the AP files of the indicated version
        threaded: It allows to run the process in a threaded way, and being in parallel or not.
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, 'convertxls.cmd')
        else:
            command = Path(self.ap_bin_str, 'convertXLS.ksh')
        command_str = str(command)
        excel_types = ['.xls', '.xlsx', '.xlsm', '.xlt', '.xltm']
        if not self.ap_version:
            return -1
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            ap_exists = False
            return -1
        files_str = str(Path(files_path_str))
        excel_config_str = str(Path(excel_config_path))
        full_command = [command_str, r'{}'.format(files_str), '-s', r'{}'.format(excel_config_str), 
                              '-l', lang_iso]
        if Path(excel_config_path).exists() and Path(excel_config_path).is_file() \
          and ap_exists:
            logging.debug('Convert_excel_to_txml: Full command: {0}'.format(
                  full_command))
            if not show_console:
                command_run = Popen(full_command, creationflags=_no_console)
            else:
                command_run = Popen(full_command)
            if Path(files_path_str).exists() and not threaded:
                command_run.wait()
            if Path(files_path_str).exists() and Path(files_path_str).is_file() and threaded:
                logging.debug('Convert_excel_to_txml: Full command: {0}'.format(
                  full_command))
                Popen(full_command, creationflags=_no_console)
            if Path(files_path_str).exists() and Path(files_path_str).is_dir() and threaded:
                excel_files_list = []
                for extension in excel_types:
                    excel_files_list.extend(Path(files_path_str).glob(extension))
                for file in excel_files_list:
                    full_command = [command_str, str(file), -'s', str(excel_config_path), 
                                         '-l', lang_iso]
                    if not show_console:
                        Popen(full_command, creationflags=_no_console)
                    else:
                        Popen(full_command)
        print(f'The file to be added to the group is: {files_path_str} {self.ap_version}')
        wf_files = _add_wf_files_to_group(files_path_str, self.ap_version)
        return wf_files
   
    def convert(self, files_path: str, config: str=None):
        pass

class Segmenter:
    """
    Generate a Segmenter class with all the properties and methods required to segment a file or files.
    
    Attributes:
        config_path (Path): Path to the config_path used for segmenter purposes.
        ap_version (str): AP version (TXLF or TXML)
        ap_path (Path): Path to the ap commands.
        ap_bin_path (Path): Path to the bin where are the commands.
        os (str): Version of the platform/os: It can be 'Linux', 'Windows' or 'MacOS'
    """
    def __init__(self, config=None, ap_version='txml', ap_path=''):
        self.config_path = None
        self.config_str = None
        if config:
            if Path(config).exists() and Path(config).is_file():
                self.config_path = Path(config)
                self.config_str = str(self.config_path)
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()
    
    def segment(self, files_path_str, threaded=False):
        """
        Segments the txml file with a config file to generate the comments/notes.
        Run the command segmentTXML of the indicated AP. 
        It only supports segmentTXML. SegmentTXLF is not implemented.
        Run:
        segment_wf(files_path_str, config_path_str, 
                    ap_path_str, threaded=False) return ErrorCode
        
        Arguments:
        files_path_str: path to the files to segment. 
        ap_path_str: Path to the AP tool
        ap_version: txlf or txml as options
        
        Return:
        -1: AP does not exist
        """
        command = Path(self.ap_bin_str, 'segment{}.cmd'.format(self.ap_version))
        command_str = str(command)
        path_files = Path(files_path_str)
        if Path(self.ap_path_str).exists():
            ap_exists = True
        else:
            logging.critical('segment_wf: AP path does not exist {}'.format(
                            self.ap_path_str))
            ap_exists = False
            return -1
        
        if ap_exists  and path_files.exists():
            files_to_process=''
            if not threaded:
                files_to_process = files_path_str
                if self.config_path:
                    full_command = [command_str,
                                '-c',
                                str(self.config_path_str),
                                str(files_to_process),
                                '-f']
                else:
                    full_command = [command_str,
                                str(files_to_process),
                                '-f']
                run(full_command, creationflags=_no_console)
                #print(f'The command to segment is: {full_command}')
                #run(full_command)
            if path_files.is_dir() and threaded:
                for file in path_files.glob('*.{}'.format(self.ap_version)):
                    files_to_process = str(file)
                    if self.config_path:
                        full_command = [
                                        command_str,
                                        '-c',
                                        str(self.config_path_str),
                                        str(files_to_process),
                                        '-f']
                    else:
                        full_command = [
                                        command_str,
                                        str(files_to_process),
                                        '-f']
                    Popen(full_command, creationflags=_no_console)
                    #print(f'The command to segment is: {full_command}')
                    #Popen(full_command)
            wf_segmented_files = _add_wf_files_to_group(path_files, self.ap_version)
        return wf_segmented_files
  
class PseudoTranslator:
    """
    This class controls the pseudotranslation of the files.
    """
    def __init__(self, config: str=None, ap_version: str='txml', ap_path: str=''):
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.config_path = None
        self.config_str = None
        if config:
            if Path(config).exists() and Path(config).is_file():
                self.config_path = Path(config)
                self.config_str = str(self.config_path)
        self.ap_version = ap_version
        self.os = platform.system()
    
    def pseudotranslate(self, files_path_str: str, threaded: bool=False):
        """
        Pseudotranslate the files in the path.
        
        Arguments:
            
        """
        if self.os == 'Windows':
            command = Path(self.ap_bin_str, f'pseudotranslate{self.ap_version}.cmd')
        else:
            command = Path(self.ap_bin_str, f'pseudotranslate{self.ap_version}.ksh')
        if self.config_str:
            command_str = str(command)
        else:
            command_str = str(command)
        files_path = Path(files_path_str)
        if Path(self.ap_bin_str).exists():
            ap_exists = True
        else:
            ap_exists = False
            return -1
        if ap_exists and Path(self.ap_bin_str).exists():
            if not threaded:
                files_to_process = files_path_str
                if self.config_str:
                    full_command = [command_str,
                                         '-c',
                                         self.config_str, 
                                         str(files_to_process)]
                else:
                    full_command = [command_str, 
                                         str(files_to_process)]
                if self.ap_version == 'txlf':
                    full_command.append('-b')
                run(full_command, creationflags=_no_console)
            if files_path.is_dir() and threaded:
                for file in files_path.glob('*.' + self.ap_version):
                    files_to_process = str(file)
                    if self.config_str:
                        full_command = [command_str, 
                                             '-c', 
                                             self.config_str, 
                                             str(file)]
                    else:
                        full_command = [command_str,
                                             str(file)]
                    if self.ap_version == 'txlf':
                        full_command.append('-b')
                    Popen(full_command, creationflags=_no_console)
        wf_pseudotranslated_files = _add_wf_files_to_group(files_path, self.ap_version)
        return wf_pseudotranslated_files

def extract_frequents(files_path_str, unk_dir_str,  ap_path_str, ap_version, 
                        src_lang, tgt_lang, threaded=False, include_hundred=True, 
                        one_file=False, tm=''):
    """
    Create frequent files from the indicated path. Folder or file. It generates
    a unique frequent file.
    run:
        create_frequents(files_path_str, unk_dir_str, ap_pat_str,
                            src_lang, tgt_lang, threaded=False) return result
    Arguments:
    files_path_str: Path to the files to process to generate the UNK files.
    unk_dir_str: String path to the files where store the UNK files.
    ap_path_str: Path to AP version to run the command extractfrequents.
    ap_version: TXLF or TXML as values.
    src_lang: Source language of the frequents.
    tgt_lang: Target language of the frequents..
    threaded: Indicate if the process needs to be run in a threaded mode.
    
    Return:
        If success:
            frequents_wf (WfGroup): Return a WfGroup object containing all the frequents for this lang-pair.
        If failure:
            -1: If the folder to store the UNK files is not valid.
            -2: If the AP is not valid or does not exist.
            -3: UNK files don't exist
    """
    os_version = platform.system()
    if os_version == 'Windows':
        command = Path(ap_path_str, 'extractfrequents.cmd')
    else:
        command = Path(ap_path_str, 'extractfrequents.ksh')
    command_str = str(command)
    files_path = Path(files_path_str)
    unk_dir = Path(unk_dir_str)
    tgt_lang = tgt_lang.replace('_', '-')
    if not unk_dir.exists():
        try:
            unk_dir.mkdir(parents=True, exist_ok=True)
        except OSError:
            logging.debug('create_frequents: Invalid folder to store UNK: {}'.format(
                    unk_dir_str))
            return -1
        except FileExistsError:
            pass
    if Path(ap_path_str).exists():
        
        ap_exists = True
    else:
        logging.critical('create_frequents: AP path does not exist {}'.format(
                        ap_path_str))
        ap_exists = False
        return -2
    if ap_exists and Path(ap_path_str).exists():
        files_to_process = files_path_str
        if files_path.exists() and not threaded and ap_version.upper() == 'txml'.upper():
            full_command = [command_str,
                        str(files_to_process),
                        '-n', '1000', 
                        '-s', src_lang, 
                        '-o', tgt_lang, 
                        '-B', str( unk_dir_str), 
                        '-m', '1', 
                        '-b', tgt_lang + '-']
            logging.debug('create_frequents: The command is: {}'.format(
                        full_command))
            #run(full_command, shell=True)
        elif files_path.exists() and not threaded and ap_version.upper() == 'txlf'.upper():
            full_command = [command_str,
                        str(files_to_process),
                        '-n', '1000', 
                        '-l', src_lang, 
                        '-p', tgt_lang, 
                        '-m', '1', 
                        '-b', tgt_lang + '-']
        if include_hundred:
            full_command.append('-i')
        if one_file:
            full_command.append('-1')
        if tm:
            full_command.append('-t')
            full_command.append(tm)
        if ap_version.upper() == 'txml'.upper():
            run(full_command, shell=True, creationflags=_no_console)
        else:
            command_run = Popen(full_command, shell=True, creationflags=_no_console, cwd=str(unk_dir))
            command_run.wait()
    unk_files = list(Path(unk_dir_str, tgt_lang).glob('*.' + ap_version))
    frequents_wf = WfGroup()
    if unk_files > 0:
        for file in unk_files:
            frequents_wf.append(Wf(str(file)))
        return frequents_wf
    else:
        return -3
        
class Cleanup:
    """
    Cleanup the files to get the translated files
    """
    excel_file_ext = ('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm')
    doc_file_ext = ('.doc', '.docx', '.docm', '.dot',
                      '.dotm', '.rtf')
    xml_file_ext = ('.xml', '.dita', '.tag')
    ppt_file_ext = ('.ppt', '.pptx', '.pptm')
    xlf_file_ext = ('.xlf', '.xliff', '.sdlxliff')
    txt_file_ext = ('.json', 'yml', '.txt')
    html_file_ext = ('.html', '.htm', '.chm')
    office_files_ext =('.xls', '.xlsx', '.xlsm', '.xlt', '.xltm',
                            '.doc', '.docx', '.docm', '.dot', '.dotm', '.rtf', 
                            '.ppt', '.pptx', '.pptm')
    
    def __init__(self, ap_version, ap_path):
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.os = platform.system()
    
    def merge(self, source_file, wf_file, threaded=False):
        """
        Method for the merge of different formats like Excel, Word,...
        Arguments:
            source_file: Source file
            wf_file: WordFast file
        """
        source_file_path = Path(source_file)
        source_file_str = str(source_file_path)
        wf_file_path = Path(wf_file)
        wf_file_str = str(wf_file_path)
        if source_file_path.suffix in self.office_files_ext:
            if source_file_path.suffix in self.excel_file_ext:
                merge_suffix = 'XLS'
            if source_file_path.suffix in self.doc_file_ext:
                merge_suffix = 'DOC'
            if source_file_path.suffix in self.ppt_file_ext:
                merge_suffix = 'PPT'
        if self.ap_path.exists():
            command = Path(self.ap_bin_path, f'merge{merge_suffix}.cmd')
            command_str = str(command)
            if source_file_path.is_file() and wf_file_path.is_file():
                full_command = [command_str, source_file_str, wf_file_str]
            else: 
                return -2
            if not threaded:
                command_run= Popen(full_command, shell=False, creationflags=_no_console, cwd=str(wf_file_path.parent))
                command_run.wait()
            else:
                Popen(full_command, shell=False, creationflags=_no_console, cwd=str(wf_file_path.parent))
        else:
            return -1

class Analysis:
    """
    Analyse the files with a TM or without
    """
    def __init__(self, ap_version: str, ap_path: str):
        """
        Initialize the object Analysis with the AP and Ap version
        """
        self.ap_version = ap_version
        self.ap_path = Path(ap_path)
        self.ap_path_str = str(self.ap_path)
        self.ap_bin_path = Path(self.ap_path, 'bin')
        self.ap_bin_str = str(self.ap_bin_path)
        self.tm = []
    
    def add_tm(self, src_lang: str, tgt_lang: str, tm_string: str) -> None:
        """
        Add a TM to the object
        """
        tm = {}
        tm['source'] = src_lang
        tm['target'] = tgt_lang
        tm['string'] = tm_string
        self.tm.append(tm)
        

if __name__ == '__main__':
    excel_str = r'c:\Programming\Python\excel_config_generator\files\ArtWorks_Pampers\aa_Master\MCCLCC_DO_Pure with CU v1 5-15-2018.xls'    
    config_str = r'c:\Programming\Python\excel_config_generator\files\ArtWorks_Pampers\MCCLCC_DO_Pure with CU v1 5-15-2018.xml'
    excel_path = Path(excel_str)
    config_path = Path(config_str)
    ap_version = 'txlf'
    ap_str = r'c:\Program Files (x86)\Analysis Package\AP4'
    ap2_str =  r'c:\Program Files (x86)\Analysis Package'
    ap_path = Path(ap_str)
    ap2_path= Path(ap2_str)
    converter = Converter(ap_path, ap_version)
    txlf_result = converter.convert_excel_to_wf(excel_str, config_path, 'EN', False)
    print(txlf_result)
    for txlf in txlf_result:
        print(txlf)
