from setuptools import setup
setup(name='wf_library', 
        version='0.9.9.18', 
        description='Library to process WF files and WF TM.', 
        long_description='It allows to process WF TXLF and TXML and txt, tmx TMs', 
        classifiers=[
            'Development Status :: 3 - Alpha', 
            'License :: OSI Approved :: LGPL License 3', 
            'Programming Language :: Python :: 3.7.0', 
            'Topic :: Text Processing :: Translation'
        ], 
        keywords='wf_library tm ap', 
        author='Llorenç Suau', 
        author_email='lsuau@translations.com', 
        license='LGPL 3', 
        packages=['wf_library'], 
        zip_safe=False)
